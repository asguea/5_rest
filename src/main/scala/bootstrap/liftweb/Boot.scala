package bootstrap.liftweb

import ca.polymtl.log4420._
import lib.RestMongo
import fixture.CheminementFixture
import rest.{RestSession2, RestSession, RestCheminement}

import net.liftweb._
import http.LiftRules

class Boot 
{
	def boot()
	{
    RestMongo.start()
    CheminementFixture.insert()

    LiftRules.dispatch.append( RestCheminement )
    LiftRules.dispatch.append( RestSession )
    LiftRules.dispatch.append( RestSession2 )

		LiftRules.early.append( _.setCharacterEncoding("UTF-8") )
	}
}